import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import { Movie, Movies } from '../lib/interfaces';
import { reducer } from './reducer';

export const exampleInitialState = {
  movies: [] as Movies,
  movie: {} as Movie,
  text: '' as string,
  isFetching: false,
  error: null as string | null,
  searchBy: 'title' as string,
  sortBy: 'release_date' as string,
};

export type State = typeof exampleInitialState;

export function initializeStore(initialState = exampleInitialState) {
  return createStore(
    reducer,
    initialState,
    composeWithDevTools(applyMiddleware(thunkMiddleware))
  )
}

export type Store = ReturnType<typeof initializeStore>