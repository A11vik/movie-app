import { exampleInitialState, State } from "./store";
import { Actions, UPDATE_TEXT, GET_MOVIES_REQUEST, GET_MOVIES_SUCCESS, GET_MOVIE_SUCCESS, GET_MOVIES_FAILURE, SORT_BY, SEARCH_BY } from "./actions";

export const reducer = (state = exampleInitialState, action: Actions): State => {
  switch (action.type) {
    case UPDATE_TEXT:
      return { ...state, text: action.payload };

    case GET_MOVIES_REQUEST:
      return { ...state, isFetching: true };

    case GET_MOVIES_SUCCESS:
      return { ...state, movies: action.payload.movies, isFetching: false, text: '' };

    case GET_MOVIE_SUCCESS:
      return { ...state, movie: action.payload, isFetching: false };

    case GET_MOVIES_FAILURE:
      return { ...state, isFetching: false, error: action.payload };

    case SORT_BY:
      return { ...state, sortBy: action.payload };

    case SEARCH_BY:
      return { ...state, searchBy: action.payload };

    default:
      return state
  }
};
