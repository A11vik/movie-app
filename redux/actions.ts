import { Movies, Movie } from "../lib/interfaces"
import { fetchCurrentMovie, fetchSearchMovies, fetchMoviesList } from "../lib/api"
import { ThunkAction } from "redux-thunk"
import { Dispatch } from "react"
import { AnyAction } from "redux"
import { Store } from '../redux/store'

interface UpdateTextAction {
  type: typeof UPDATE_TEXT,
  payload: string
};

interface SortByAction {
type: typeof SORT_BY,
payload: string
};
interface GetMoviesRequestAction {type: typeof GET_MOVIES_REQUEST};
interface GetMoviesSuccessAction {
type: typeof GET_MOVIES_SUCCESS,
payload: { movies: Movies }
}
interface GetMovieSuccessAction {
type: typeof GET_MOVIE_SUCCESS,
payload: Movie
}
interface GetMoviesFailureAction {
type: typeof GET_MOVIES_FAILURE,
payload: null | string
}
interface SearchByAction {
type:   typeof SEARCH_BY,
payload: string
}

export type Actions = UpdateTextAction | SortByAction | GetMoviesRequestAction
  | GetMoviesSuccessAction | GetMovieSuccessAction | GetMoviesFailureAction | SearchByAction

export const UPDATE_TEXT = 'UPDATE_TEXT'
export const GET_MOVIES_REQUEST = 'GET_MOVIES_REQUEST'
export const GET_MOVIES_SUCCESS = 'GET_MOVIES_SUCCESS'
export const GET_MOVIES_FAILURE = 'GET_MOVIES_FAILURE'
export const GET_MOVIE_SUCCESS = 'GET_MOVIE_SUCCESS'
export const SEARCH_BY = 'SEARCH_BY'
export const SORT_BY = 'SORT_BY'

// ACTIONS

export const moviesRequest = (): Actions => ({
  type: GET_MOVIES_REQUEST
});

export const moviesSuccess = (movies: Movies): Actions => ({
  type: GET_MOVIES_SUCCESS,
  payload: { movies }
});

export const moviesFailure = (err: string): Actions => ({
  type: GET_MOVIES_FAILURE,
  payload: err
});

export const movieSuccess = (movie: Movie): Actions => ({
  type: GET_MOVIE_SUCCESS,
  payload: movie
});

export const updateText = (text: string): Actions => {
  return { type: UPDATE_TEXT, payload: text };
};

export const sortBy = (param: string): Actions => {
  return { type: SORT_BY, payload: param };
};

export const searchBy = (param: string): Actions => {
  return { type: SEARCH_BY, payload: param };
};

// ASYNC Actions

export type DispatchType = Dispatch<AnyAction>
export type ThunkType<R> = ThunkAction<R, Store, unknown, Actions>

export const getMovies = (str: null | string = null, searchByParam: string = 'title'): ThunkType<Promise<void>> => async dispatch => {
  dispatch(moviesRequest());
  try {
    const movies: Movies = str
      ? await fetchSearchMovies(str, searchByParam)
      : await fetchMoviesList();

    dispatch(moviesSuccess(movies));
  } catch (e) {
    dispatch(moviesFailure(e));
  }
};

export const getMovieById = (id: string): ThunkType<Promise<void>> => async dispatch => {
  dispatch(moviesRequest());
  try {
    const movie: Movie = await fetchCurrentMovie(id);
    const genres: string = movie.genres[0];
    const movies: Movies = await fetchSearchMovies(genres, 'genres');

    dispatch(movieSuccess(movie));
    dispatch(moviesSuccess(movies));
  } catch (e) {
    dispatch(moviesFailure(e));
  }
};