import * as React from 'react';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import Router from 'next/router';
import { State } from '../../redux/store';
import { getMovies, updateText, sortBy, searchBy } from '../../redux/actions';
import _ from 'lodash';

import Header from '../../components/Header';
import SearchForm from '../../components/SearchForm';
import SortInfo from '../../components/SortInfo';
import MoviesContainer from '../../components/MoviesContainer';
import Footer from '../../components/Footer';
import { Movies, Context, DispatchType } from '../../lib/interfaces';

type SearchProps = {
  movies: Movies,
  text: string,
  isFetching: boolean,
  sortBy: string,
  searchParams: string,
  dispatch?: DispatchType
}

const getMoviesSelector = (state: State): Movies => state.movies;
const getSortInfoSelector = (state: State): string => state.sortBy;

const sortMovie = createSelector(
  getMoviesSelector,
  getSortInfoSelector,
  (movies: Movies, sortBy: string): Movies => _.sortBy(movies, sortBy),
);

const mapStateToProps = (state: State): SearchProps => {
  const sortMovies: Movies = sortMovie(state);
  return {
    movies: sortMovies,
    isFetching: state.isFetching,
    text: state.text,
    sortBy: state.sortBy,
    searchParams: state.searchBy
  };
};

class Search extends React.Component<SearchProps> {
  static async getInitialProps({ store, query }: Context) {
    await store.dispatch(getMovies(query.search));
    return { search: query.search };
  }

  handleInputText = (e: React.ChangeEvent<HTMLInputElement>): void => {
    const { dispatch } = this.props;
    dispatch(updateText(e.target.value))
  }

  handleSubmit = (e: React.FormEvent): void => {
    e.preventDefault();
    const { text, searchParams } = this.props;
    Router.push('/search/[search]', `/search/${text}&searchBy=${searchParams}`, { searchText: text, searchParams: searchParams });
  }

  handleClickSortBy = (param: string) => (e: React.MouseEvent): void => {
    e.preventDefault();
    const { dispatch } = this.props;
    dispatch(sortBy(param));
  }

  handleClickSearchBy = (param: string) => (e: React.MouseEvent): void => {
    e.preventDefault();
    const { dispatch } = this.props;
    dispatch(searchBy(param))
  }

  render () {
    const { movies, isFetching, text, sortBy, searchParams } = this.props;
    return (
      <div className="container">
        <Header />
        <SearchForm
          text={text}
          handleInputText={this.handleInputText}
          handleSubmit={this.handleSubmit}
          searchParams={searchParams}
          onClickSearchParams={this.handleClickSearchBy}
        />
        <SortInfo
          moviesLength={movies.length}
          sortBy={sortBy}
          handleClickSortBy={this.handleClickSortBy}
        />
        <MoviesContainer movies={movies} isFetching={isFetching} />
        <Footer />

        <style jsx global>{`
          body {
            margin: 0;
            padding: 0;
            background-color: #e0e0e0;
            color: #ffffff;
            font-family: 'Roboto', sans-serif;
          }
          
          a {
            text-decoration: none;
          }
          
          .container {
            max-width:  1440px;
            margin: 0 auto;
            background-color: #757575;
          }
        `}</style>
      </div>
    );
  }
}

export default connect(mapStateToProps)(Search);
