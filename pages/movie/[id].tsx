import React from 'react';
import { connect } from 'react-redux';
import { State } from '../../redux/store';
import { getMovieById } from '../../redux/actions';
import { Movie, Context } from '../../lib/interfaces';

import Header from '../../components/Header';
import SortInfo from '../../components/SortInfo';
import MoviesContainer from '../../components/MoviesContainer';
import Footer from '../../components/Footer';

type PropsType = {
  movie: Movie,
  movies: Movie[],
  isFetching: boolean
};

const mapStateToProps = (state: State): PropsType => ({
  movie: state.movie,
  movies: state.movies,
  isFetching: state.isFetching,
});

class MoviePage extends React.Component<PropsType> {
  static async getInitialProps(ctx: Context) {
    console.log(ctx)
    await ctx.store.dispatch(getMovieById(ctx.query.id));
  }

  render() {
    const { isFetching } = this.props;

    if (isFetching) {
      return <div>LOADING</div>;
    }

    const { movie, movies } = this.props;
    const date: string = movie.release_date && movie.release_date.split('-')[0];
    return (
      <div className="container">
        <Header currentPage='movie' />
        <section className="movie">
          <img className="movie__image" src={movie.poster_path} alt="poster" />
          <div>
            <h2 className="movie__title">
              {movie.title} <span className="movie__vote">{movie.vote_average}</span>
            </h2>
            <p className="movie__descr-short">{movie.tagline}</p>
            <p className="movie__params">
              <span className="movie__year">{date} year &nbsp;</span>
              <span className="movie__length">{movie.runtime} min</span>
            </p>
            <p className="movie__description">{movie.overview}</p>
          </div>
        </section>
        <SortInfo genre={movie} currentPage={'film'} />
        <MoviesContainer movies={movies} />
        <Footer />

        <style jsx global>{`
          body {
            margin: 0;
            padding: 0;
            background-color: #e0e0e0;
            color: #ffffff;
            font-family: 'Roboto', sans-serif;
          }
          
          a {
            text-decoration: none;
          }
          
          .container {
            max-width:  1440px;
            margin: 0 auto;
            background-color: #757575;
          }
        `}</style>
        <style jsx>{`
          .movie {
            display: flex;
            padding: 0 5%;
            padding-top: 5%;
            padding-bottom: 3%;
            background-color: #232323;
          }
          
          .movie__image {
            margin-right: 5%;
          
            max-width: 300px;
          }
          
          .movie__title {
            font-size: 2.5rem;
          }
          
          .movie__vote {
            padding: 5px;
            border: 1px solid white;
            border-radius: 50%;
            font-size: 1.5rem;
          }
          
          @media (max-width: 740px) {
            .movie {
              flex-direction: column;
              align-items: center;
            }
          }
        `}</style>
      </div>

    );
  }
}

export default connect(mapStateToProps)(MoviePage);
