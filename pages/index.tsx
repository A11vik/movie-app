import React from 'react';
import Router from 'next/router';
import { connect } from 'react-redux';
import { State } from '../redux/store';
import { updateText , sortBy, searchBy } from '../redux/actions';

import Header from '../components/Header';
import SearchForm from '../components/SearchForm';
import SortInfo from '../components/SortInfo';
import MoviesContainer from '../components/MoviesContainer';
import Footer from '../components/Footer';
import { DispatchType, Movies } from '../lib/interfaces';

type Props = {
  movies: Movies,
  isFetching: boolean,
  text: string,
  sortBy: string,
  searchParams: string,
  dispatch?: DispatchType
}

const mapStateToProps = (state: State): Props => {
  return {
    movies: state.movies,
    isFetching: state.isFetching,
    text: state.text,
    sortBy: state.sortBy,
    searchParams: state.searchBy
  }
};

class Home extends React.Component<Props> {

  handleInputText = (e: React.ChangeEvent<HTMLInputElement>): void => {
    const { dispatch } = this.props;
    dispatch(updateText(e.target.value))
  }

  handleSubmit = (e: React.FormEvent): void => {
    e.preventDefault();
    const { text, searchParams } = this.props;
    console.log(Router)
    Router.push('/search/[search]', `/search/${text}&searchBy=${searchParams}`, { searchText: text, searchParams: searchParams });
  }

  handleClickSortBy = (param: string) => (e: React.MouseEvent) => {
    e.preventDefault();
    const { dispatch } = this.props;
    dispatch(sortBy(param));
  }

  handleClickSearchBy = (param: string) => (e: React.MouseEvent<HTMLButtonElement>) => {
    e.preventDefault();
    const { dispatch } = this.props;
    dispatch(searchBy(param))
  }

  render () {
    const { movies, isFetching, text, sortBy, searchParams } = this.props;
    return (
      <div className="wrapper">
        <div className="container">
          <Header />
          <SearchForm
            text={text}
            handleInputText={this.handleInputText}
            handleSubmit={this.handleSubmit}
            searchParams={searchParams}
            onClickSearchParams={this.handleClickSearchBy}
          />
          <SortInfo
            sortBy={sortBy}
            handleClickSortBy={this.handleClickSortBy}
          />
          <MoviesContainer
            movies={movies}
            isFetching={isFetching}
          />
          <Footer />
        </div>

        <style jsx global>{`
          body {
            margin: 0;
            padding: 0;
            background-color: #e0e0e0;
            color: #ffffff;
            font-family: 'Roboto', sans-serif;
          }
          
          a {
            text-decoration: none;
          }
          
          .container {
            max-width:  1440px;
            margin: 0 auto;
            background-color: #757575;
          }
        `}</style>
      </div>
    )
  }
}

export default connect(mapStateToProps)(Home);
