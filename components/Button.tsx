import React from 'react';

interface ButtonProps {
  text: string,
  onClick: (e: React.MouseEvent) => void,
  isActive: boolean
}

const Button: React.FC<ButtonProps> = ({ text = 'button', onClick = () => {return}, isActive = false }) => {
  return (
    <a onClick={onClick} href="#" className="btn">
      {text}

      <style jsx>{`
        .btn {
          padding: 10px 40px;
          background-color: ${isActive ? '#f65261' : '#555555'};
          color: #ffffff;
          opacity: ${isActive ? 0.9 : 0.7};
        }

        .btn:hover {
          background-color: ${isActive ? '#f65261' : '#f8bbd0'};
        }
      `}</style>
    </a>
  );
};

export default Button;
