import React from 'react';
import Link from 'next/link';

const Logo = () => (
  <div>
    <Link href="/">
      <a className="logo">
        <b className="logo__bold">netflix</b>roulette
      </a>
    </Link>

    <style jsx>{`
      .logo {
        color: #f65261;
        font-size: 1.5rem;
        cursor: pointer;
      }
    `}</style>
  </div>
);

export default Logo;
