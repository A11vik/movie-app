import * as React from 'react';
import Link from 'next/link';
import MovieCard from './MovieCard';
import { Movie, Movies } from '../lib/interfaces';

type Props = {
  movies: Movies,
  isFetching?: boolean
}

const renderError = () => <p className="film-list__empty">No films found</p>;

const renderMovies = (movies: Movies) => movies.map((movie: Movie) => <Link href={`/movie/[id]`} as={`/movie/${movie.id}`} key={movie.id} >
    <a>
      <MovieCard movie={movie} />
    </a>
    </Link>);

const MoviesContainer: React.FC<Props> = (props: Props) => {
  const { movies = [], isFetching = false } = props;
  return (
    <section className="films">
      {isFetching
        // ? <Loader type="Oval" color="#ffffff" height={180} width={180} />
        ? <div>LOADING...</div>
        : <ul className="films__list film-list">
            {movies.length === 0 ? renderError() : renderMovies(movies)}
          </ul>}

      <style jsx>{`
        .films {
          padding-top: 5%;
          background-color: #232323;
        }
        
        .films__list {
          display: flex;
          justify-content: space-between;
          flex-wrap: wrap;
        
          list-style: none;
        }
        
        .film-list__empty {
          width: 100%;
          font-size: 2.5rem;
          text-align: center;
        }
        
        @media (max-width: 1000px) {
          .films__list {
            justify-content: center;
          }
        }
        
      `}</style>
    </section>
  );
};

export default MoviesContainer;
