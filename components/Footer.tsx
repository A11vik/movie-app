import React from 'react';
import Logo from './Logo';

const Footer: React.FC = () => (
  <footer className="page-footer">
    <Logo />

    <style jsx global>{`
      .page-footer a {
        display: flex;
        justify-content: center;
      }
    `}</style>
  </footer>
);

export default Footer;
