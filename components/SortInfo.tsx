import React from 'react';

import Button from './Button';
import { Movie } from '../lib/interfaces';

type Props = {
  moviesLength?: number,
  sortBy?: string,
  handleClickSortBy?: (a: string) => (e: React.MouseEvent) => void,
  currentPage?: string,
  genre?: Movie,
}

const SortInfo: React.FC<Props> = (props: Props) => {
  const {
    moviesLength = 10, sortBy = 'release_date', handleClickSortBy,
    currentPage = 'main',
  } = props;

  if (currentPage === 'main') {
    return (
        <section className="sort">
          <p className="sort__count count">movie found: <span>{moviesLength}</span></p>
          <div className="sort__sort-by sort-by">
            <span className="sort-by__title">Sort by</span>
            <Button
              onClick={handleClickSortBy('release_date')}
              isActive={sortBy === 'release_date'}
              text="Release date"
            />
            <Button
              onClick={handleClickSortBy('vote_average')}
              isActive={sortBy === 'vote_average'}
              text="Rating"
            />
          </div>

          <style jsx>{`
            .sort {
              margin: 0 5%;
              padding-top: 2%;
              padding-bottom: 2%;
              display: flex;
              align-items: center;
              justify-content: space-between;
            }
            
            .sort-by {
              display: flex;
              align-items: center;
              text-transform: uppercase;
            }
            
            .sort-by__title {
              margin-right: 10px;
            }
            
            .sort-by .btn:first-of-type {
              border-top-left-radius: 5px;
              border-bottom-left-radius: 5px;
            }
            
            .sort-by .btn:last-of-type {
              border-top-right-radius: 5px;
              border-bottom-right-radius: 5px;
            }
            
            @media (max-width: 740px) {
              .sort {
                flex-direction: column;
              }
            }
            
            @media (max-width: 470px) {
              .sort-by {
                flex-direction: column;
              }
            }
          `}</style>
        </section>
    );
  }
  const { genre } = props;
  return (
    <section className="sort">
      <p className="sort__count count">Films by {genre && genre.genres ? genre.genres[0] : null}</p>

      <style jsx>{`
        .sort {
          margin: 0 5%;
          padding-top: 2%;
          padding-bottom: 2%;
          display: flex;
          align-items: center;
          justify-content: space-between;
        }
      `}</style>
    </section>
  );
};

export default SortInfo;
