import React from 'react';

import Button from './Button';

type Props = {
  handleSubmit: (e: React.FormEvent) => void,
  handleInputText: (e: React.ChangeEvent<HTMLInputElement>) => void,
  text: string,
  onClickSearchParams: (searchParams: string) => (e: React.MouseEvent) => void,
  searchParams: string
}

const SearchForm: React.FC<Props> = (props: Props) => {
  const {
    handleSubmit = () => {}, handleInputText = () => {}, text = '',
    onClickSearchParams = () => () => {}, searchParams = 'title',
  } = props;
  return (
    <section className="search">
      <div className="search__wrapper">
        <form onSubmit={handleSubmit} className="search__form form">
          <label className="form__title">Find your movie</label>
          <div className="form__container">
            <input
              onChange={handleInputText}
              id="forms"
              className="form__input"
              type="text"
              value={text}
            />
            <input className="form__btn" type="submit" value="Search" />
          </div>
        </form>
        <div className="search__params params">
          <span className="params__title">Search by</span>
          <Button onClick={onClickSearchParams('title')} text="Title" isActive={searchParams === 'title'} />
          <Button onClick={onClickSearchParams('genres')} text="Genre" isActive={searchParams === 'genres'} />
        </div>
      </div>

      <style jsx>{`
        .search {
  
          padding-top: 5%;
          padding-bottom: 2%;
        
          background-image: url('/images/bg-films1.jpeg');
          background-repeat: no-repeat;
          background-position: center;
        }
        
        .search__form {
          margin: 0 5%;
          margin-bottom: 2%;
        }
        
        .form {
          display: flex;
          flex-direction: column;
        }
        
        .form__title {
          margin-bottom: 2%;
          font-size: 2.5rem;
          text-transform: uppercase;
          color: #ffffff;
        }
        
        .form__container {
          display: flex;
          width: 100%;
          margin-bottom: 2%;
        }
        
        .form__input {
          flex-grow: 3;
          height: 50px;
          margin-right: 10px;
          padding-left: 10px;
        
          background-color: #424242;
          border: none;
          border-radius: 5px;
          color: #ffffff;
          font-size: 1.5rem;
          opacity: 0.7;
        }
        
        .form__btn {
          flex-grow: 1;
        
          background-color: #f65261;
          color: #ffffff;
          border: none;
          border-radius: 5px;
          cursor: pointer;
          text-transform: uppercase;
        }
        
        .search__params {
          margin: 0 5%;
          margin-bottom: 5%;
          text-transform: uppercase;
        }
        
        .params__title {
          margin-right: 2%;
        }
        
        .params .btn:first-of-type {
          border-top-left-radius: 5px;
          border-bottom-left-radius: 5px;
        }
        
        .params .btn:last-of-type {
          border-top-right-radius: 5px;
          border-bottom-right-radius: 5px;
        }
        
        @media (max-width: 470px) {
          .form__container {
            flex-direction: column;
            width: auto;
          }
          .form__btn {
            margin: 0 auto;
            max-width: 200px;
            padding: 10px;
          }
        
          .params {
            display: flex;
            flex-direction: column;
            align-items: center;
          }
        
          .params .btn {
            max-width: 200px;
          }
        }
      `}</style>
    </section>
  );
};

export default SearchForm;
