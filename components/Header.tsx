import React from 'react';
import Link from 'next/link'
import Logo from './Logo';

type NavbarProps = {
  currentPage?: string
}

const Navbar: React.FC<NavbarProps> = (props: NavbarProps) => {
  const { currentPage = 'main' } = props;
  return (
    <nav className="navbar">
      <Logo />
      {currentPage === 'movie' &&
        <Link href="/">
          <a href="#" className="search-icon"></a>
        </Link>}

      <style jsx>{`
        .navbar {
          position: absolute;
          display: flex;
          justify-content: space-between;
          align-items: center;
          padding-left: 2%;
          max-width: 1400px;
          width: 90%;
        }
        
        .search-icon {
          display: block;
          width: 20px;
          height: 20px;
        
          background-image: url('/images/icon-search.png');
          background-size: cover;
          filter: invert(100%);
        }
      `}</style>
    </nav>
  );
};

export default Navbar;
