import React from 'react';

type MovieProps = {
  movie: {
    title: string,
    release_date: string,
    poster_path: null | string,
    genres: string[],
  }
}

const MovieCard = (props: MovieProps) => {
  const {
    title = 'title', release_date = '2019-12-12',
    poster_path = null, genres = [],
  } = props.movie;

  const year: string = release_date.split('-')[0];

  return (
    <li className="film-list__item film">
      <img className="film__image" src={poster_path} alt="poster" />
      <div className="film__info">
        <p className="film__description">
          <span className="film__title">{title}</span>
          <br />
          <span className="film__brief">
            {genres.filter((item, idx) => idx <= 1).map((i) => <span key={i}>{i} </span>)}
          </span>
        </p>
        <span className="film__year">{year}</span>
      </div>

      <style jsx>{`
        .film-list__item {
          max-width: 300px;
          margin-right: 15px;
          opacity: 0.8;
          color: #ffffff;
        }
        
        .film__item:hover {
          opacity: 1;
        }
        
        .film__image {
          max-width: 300px;
        }
        
        .film__info {
          display: flex;
          align-items: center;
          justify-content: space-between;
        }
        
        @media (max-width: 350px) {
          .film__image {
            max-width: 250px;
          }
        }
      `}</style>
    </li>
  );
};

export default MovieCard;
