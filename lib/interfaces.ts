import { NextPageContext } from 'next';
import { Dispatch } from 'redux';
import { Actions } from '../redux/actions';

export interface Movie {
  id: number,
  title: string,
  release_date: string,
  poster_path: string,
  genres: string[],
  vote_average?: string,
  tagline?: string,
  runtime?: number,
  overview?: string
}

export type Movies = Movie[]

export interface Context extends NextPageContext {
  query: {
    id: string
    search: string
  },
  store: {
    dispatch: (cb: any) => void
  }
}

export type DispatchType = Dispatch<Actions>