import fetch from 'isomorphic-fetch';
import { Movies, Movie } from './interfaces';

interface IApi {
  baseUrl: string,
  getMovies: () => string,
  searchMovie: (str: string, searchBy: string) => string,
  getCurrentMovie: (id: string) => string
}

const api: IApi = {
  baseUrl: 'https://reactjs-cdp.herokuapp.com',
  getMovies(): string {
    return `${this.baseUrl}/movies`;
  },
  searchMovie(str: string, searchBy: string): string {
    const searchString: string = searchBy === 'genres' ? `&searchBy=genres` : '';
    return `${this.baseUrl}/movies?search=${str}${searchString}&sortBy=release_date&sortOrder=desc`;
  },
  getCurrentMovie(id: string): string {
    return `${this.baseUrl}/movies/${id}`;
  },
};

export async function fetchMoviesList(): Promise<Movies> {
  try {
    const response = await fetch(api.getMovies());
    const { data }: { data: Movies } = await response.json();
    return data;
  } catch (e) {
    console.warn(e);
    return null;
  }
}

export async function fetchSearchMovies(str: string, searchBy: string): Promise<Movies> {
  try {
    const response = await fetch(api.searchMovie(str, searchBy));
    const { data }: {data: Movies} = await response.json();
    return data;
  } catch (e) {
    console.warn(e);
    return null;
  }
}

export async function fetchCurrentMovie(id: string): Promise<Movie> {
  try {
    const response = await fetch(api.getCurrentMovie(id));
    const data: Movie = await response.json();
    return data;
  } catch (e) {
    console.warn(e);
    return null;
  }
}
