import React from 'react';
import { shallow } from 'enzyme';
import SortInfo from '../components/SortInfo';

it('should render correctly on movie page', () => {
  const props = { genre: { genres: ['Drama', 'Action'] }, currentPage: 'movie' };
  const component = shallow(<SortInfo {...props} />);

  expect(component).toMatchSnapshot();
  expect(component.find('p').text()).toEqual('Films by Drama');
  expect(component.find('Button')).toHaveLength(0);
});

it('should render correctly on main page', () => {
  const props = {
    moviesLength: 100,
    currentPage: 'main',
  };
  const component = shallow(<SortInfo {...props} />);

  expect(component).toMatchSnapshot();
  expect(component.find('p').text()).toEqual('movie found: 100');
  expect(component.find('Button')).toHaveLength(2);
});

it('should render correctly on movie page without genre', () => {
  const props = { currentPage: 'movie' };
  const component = shallow(<SortInfo {...props} />);

  expect(component).toMatchSnapshot();
  expect(component.find('p').text()).toEqual('Films by ');
});
