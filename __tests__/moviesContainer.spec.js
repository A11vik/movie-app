import React from 'react';
import { shallow, mount } from 'enzyme';
import MovieCard from '../components/MovieCard';
import MoviesContainer from '../components/MoviesContainer';

const handleClickOnMovie = jest.fn();

const props = {
  movies: [
    {
      title: 'some title',
      release_date: '2000-1-1',
      poster_path: 'https://some-url.com',
      genres: ['Drama', 'Action', 'Sport'],
      id: 1,
    },
    {
      title: 'some title2',
      release_date: '2002-1-1',
      poster_path: 'https://some-url.com',
      genres: ['Sport', 'Action'],
      id: 2,
    },
  ],
  handleClickOnMovie,
};

it('should render two movies', () => {
  const component = shallow(<MoviesContainer {...props} />);
  expect(component).toMatchSnapshot();
});

it('should render not found message', () => {
  const props = {
    movies: [],
  };

  const component = mount(<MoviesContainer {...props} />);
  expect(component.find('p').text()).toEqual('No films found');
});

it('should be two components', () => {
  const component = shallow(<MoviesContainer {...props} />);
  expect(component.find(MovieCard).length).toBe(2);
  // expect(component.find(Loader).length).toBe(0);
});

it('should be loader', () => {
  const props = { isFetching: true };

  const component = shallow(<MoviesContainer {...props} />);
  expect(component.find('div').length).toBe(1);
});
