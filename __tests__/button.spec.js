import React from 'react';
import { shallow, mount } from 'enzyme';
import Button from '../components/Button';

describe('should be rendered correctly', () => {
  it('it should be render correct text button', () => {
    const component = shallow(<Button text="text" />);
    expect(component).toMatchSnapshot();
  });
  it('should be added className active', () => {
    const component = shallow(<Button text="search" isActive={true} />);
    expect(component).toMatchSnapshot();
  });
  it('should be call function on click', () => {
    const onClick = jest.fn();
    const component = mount(<Button text="search" isActive={false} onClick={onClick} />);
    component.find('.btn').simulate('click');
    expect(onClick).toHaveBeenCalled();
  });
});
