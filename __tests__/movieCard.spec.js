import React from 'react';
import { shallow, mount } from 'enzyme';
import MovieCard from '../components/MovieCard';

it('should be rendered correct', () => {
  const movie = {
    title: 'some title',
    release_date: '2000-1-1',
    poster_path: 'https://some-url.com',
    genres: ['Drama', 'Action', 'Sport'],
  };

  const component = mount(<MovieCard movie={movie} />);
  expect(component).toMatchSnapshot();
  expect(component.find(".film__title").text()).toEqual(movie.title);
  expect(component.find(".film__brief").text()).toEqual('Drama Action ');
});
