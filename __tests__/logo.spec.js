import React from 'react';
import { shallow } from 'enzyme';
import Link from 'next/link';
import Logo from '../components/Logo';

it('should render logo ', () => {
  const component = shallow(<Logo />);
  expect(component.find(Link)).toHaveLength(1);
  expect(component.find('.logo__bold').text()).toEqual('netflix');
});