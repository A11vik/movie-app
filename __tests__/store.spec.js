import thunk from 'redux-thunk';
import configureMockStore from 'redux-mock-store';
import fetchMock from 'fetch-mock';

import { moviesRequest, moviesSuccess, moviesFailure, movieSuccess,
sortBy, searchBy, updateText, reducer } from '../store';

const movies = [{id: 1, name: 'first'}, {id: 2, name: 'second'}];
const error = {name: 'error'};
const movie = {id: 3, name: 'film'};
const text = 'some text';
const sortParam = 'rating';
const searchParam = 'genre';

const request = { type: 'GET_MOVIES_REQUEST' };
const success = { type: 'GET_MOVIES_SUCCESS', payload: { movies } };
const failure = { type: 'GET_MOVIES_FAILURE', payload: error };
const movieSuc = { type: 'GET_MOVIE_SUCCESS', payload: { movie }};
const textAction = { type: 'UPDATE_TEXT', payload: text };
const searchAction = { type: 'SEARCH_BY', payload: searchParam };
const sortAction = { type: 'SORT_BY', payload: sortParam };

it('should be return true object from actions', () => {
  expect(moviesRequest()).toEqual(request);
  expect(moviesSuccess(movies)).toEqual(success);
  expect(moviesFailure(error)).toEqual(failure);
  expect(movieSuccess(movie)).toEqual(movieSuc);
  expect(sortBy(sortParam)).toEqual(sortAction);
  expect(searchBy(searchParam)).toEqual(searchAction);
  expect(updateText(text)).toEqual(textAction);
});

it('reducer tests', () => {
  const initState = {
    movies: [],
    movie: {},
    text: '',
    isFetching: false,
    error: null,
    searchBy: 'title',
    sortBy: 'release_date',
  }

  expect(reducer(initState, { type: 'undefined'})).toEqual(initState);
  expect(reducer(initState, success)).toEqual({ ...initState, movies: success.payload.movies});
  expect(reducer(initState, failure)).toEqual({ ...initState, error: failure.payload});
});