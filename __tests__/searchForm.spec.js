import React from 'react';
import { shallow, mount } from 'enzyme';

import SearchForm from '../components/SearchForm';

it('render correctly form', () => {
  const component = shallow(<SearchForm />);
  expect(component).toMatchSnapshot();
});

it('should call to function on input change', () => {
  const handleInput = jest.fn();
  const component = mount(<SearchForm text="test" handleInputText={handleInput} />);
  component.find('#forms').simulate('change');
  expect(handleInput).toHaveBeenCalled();
});

it('should call to function on click submit', () => {
  const handleFindMovie = jest.fn();
  const component = mount(<SearchForm text="test" handleSubmit={handleFindMovie} />);
  component.find('.form').simulate('submit');
  expect(handleFindMovie).toHaveBeenCalled();
});
